<!--
SPDX-FileCopyrightText: 2022 Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# Using Nix on Fedora Silverblue

The following steps allow for running Nix on Fedora Silverblue.

Note that some of these steps are workarounds and could be improved.

## Disable SELinux

We need to disable SELinux otherwise the nix socket cannot be created.

```bash
$ sudo setenforce Permissive
$ sudo sed -i "s/^SELINUX=.*$/SELINUX=permissive/g" /etc/sysconfig/selinux
$ sudo sed -i "s/^SELINUX=.*$/SELINUX=permissive/g" /etc/selinux/config
```

This could be improved in the future

  * https://github.com/NixOS/nix/issues/2374
  * https://github.com/NixOS/nix/pull/2670

## SSL cert file

We need to set the SSL cert file for the nix-daemon as it runs in a different user which is missing this env var.

```bash
$ sudo mkdir /etc/systemd/system/nix-daemon.service.d
$ sudo tee /etc/systemd/system/nix-daemon.service.d/override.conf <<EOF
[Service]
Environment="NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-bundle.crt"
EOF
```

This could be improved in the future

  * https://github.com/NixOS/nix/issues/3155

## Create /nix mount

We need to create a systemd service which bind mounts /nix from /var/lib/nix.

This allows for Nix to use binary caches but for data to still be stored in the correct place for silverblue.

```bash
$ sudo tee /etc/systemd/system/mount-nix-prepare.service <<EOF
[Unit]
Description=Prepare nix mount points

[Service]
Type=oneshot
ExecStartPre=chattr -i /
ExecStart=/bin/sh -c "mkdir -p /nix"
ExecStart=/bin/sh -c "mkdir -p /var/lib/nix"
ExecStart=/bin/sh -c "mount --bind /var/lib/nix /nix"
# We need to restart systemd and load the nix-daemon again
# as the nix-daemon files are actually inside the /nix folder
# which hasn't been mounted until now
ExecStart=/bin/sh -c "systemctl daemon-reload"
# This will fail the first time as we haven't installed nix yet
ExecStart=-/bin/sh -c "systemctl start nix-daemon.socket"
ExecStopPost=chattr +i /

[Install]
WantedBy=local-fs.target
EOF
$ sudo systemctl daemon-reload
$ sudo systemctl enable --now mount-nix-prepare.service
```

This could be improved in the future, as currently we need to restart the systemd daemon and nix-daemon
once the bind mount has occurred. This is because the nix-daemon files are stored in /nix which isn't available yet.

## Install Nix

```bash
$ sh <(curl -L https://nixos.org/nix/install) --daemon
```

Now restart your terminal session and you can use Nix as normal on your Fedora Silverblue system.

See [nix-example](nix-example.md) for an example of setting up home-manager with Nix.
